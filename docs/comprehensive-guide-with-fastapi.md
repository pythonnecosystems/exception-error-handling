# 전문가를 위한 Python 예외와 오류 처리: FastAPI에서의 가이드<sup>[1](#footnote_1)</sup>

Python 프로젝트에서 강력한 오류 처리 기술을 이해하고 구현하면 코드 품질, 사용자 경험 및 유지 관리 가능성을 개선할 수 있다.

![](./images/0_XfadqfGO8EUX1m_1.webp)

## 소개
“**Cannot connect to the DB**” 또는 "**System Error: 500**", 어렵게 보이지요? 이는 단순한 오류가 아니라 어플리케이션을 사용하는 사용자의 경험을 망칠 수 있는 장애를 의미한다. 백엔드 개발 영역에서는 항상 원활한 사용자 여정을 제공하는 것이 목표이다. 효율적이고 반응성이 뛰어나며 직관적인 어플리케이션을 만들기 위해 최선을 다하지만, 이러한 여정을 방해할 수 있는 예기치 못한 오류로 인한 예외가 발생할 가능성을 완전히 피할 수는 없다.

이러한 예외를 막다른 골목이 아닌 단순한 장애물로 바꿀 수 있다면 어떨까요? 난해한 오류 메시지를 명확하고 간결하며 체계적으로 구성된 피드백으로 대체하면 사용자 경험과 신뢰의 연속성을 보장할 수 있다. "**현재 서버 연결에 문제가 발생하고 있습니다.잠시 후 다시 시도해 주세요.**"와 같이 설명을 변경하는 것만으로도 큰 차이를 만들 수 있다. 이러한 접근 방식은 사용자에게 문제에 대해 알리고 일시적인 결함이며 해결 중이라는 사실을 알려 사용자를 안심시킬 수 있다. 따라서 Python 예외 처리와 FastAPI를 살펴보면서 예외가 발생하였을 때 사용자 경험을 향상시키는 데 초점을 맞춰 보겠다. 결국 **예외 처리 기술은 단순히 오류를 포착하는 것이 아니라 어플리케이션이 성공할 때처럼 실패할 때도 우아하게 대응할 수 있도록 하는 것이다**. 시작해 봅시다!

### Python에서 예외 처리의 중요성
Python의 예외 처리는 강력하고 탄력적인 어플리케이션을 구축하는 데 중요한 역할을 한다. 이를 통해 개발자는 런타임 오류를 예측, 감지 및 해결할 수 있으므로 소프트웨어가 갑작스럽게 종료되거나 예측할 수 없는 동작을 일으키지 않고 원활하게 실행되도록 보장할 수 있다.

### 오류 타입(구문, 런타임, 논리)
Python의 오류는 크게 세 가지 타입으로 분류할 수 있다.

1. **구문 오류**: 구문 오류는 구문 분석기가 구문 오류를 발견할 때 발생한다.
2. **런타임 오류**: 프로그램 실행 중에 감지된 오류로 예외라고도 한다.
3. **논리 오류**: 프로그램의 논리 또는 알고리즘에서 발생하는 오류이다.

### 일반적인 오류 메시지와 그 의미
Python에는 오류 메시지를 생성하는 내재된 다양한 예외가 있다. 예를 들어, 이름을 찾을 수 없는 경우 `NameError`, 연산이나 함수가 부적절한 타입의 객체에 적용된 경우 `TypeError`, 함수의 인수가 올바른 타입이지만 부적절한 값인 경우 `ValueError`를 발생시킨다.

이 [Colab Notebook](https://colab.research.google.com/drive/1k1Lkq2QRdMJslcBc3niuCmsVydt-4L0u#scrollTo=saN_FK9R84n_)을 사용해 아래 코드 대부분을 따라해 보자.

### `try-except` 블럭 이해
파이썬은 `try-except` 블록을 사용하여 예외를 처리한다.

아래 스크립트를 실행하려고 하면

```python
x = 1 / 0
```

오류 응답이 나타난다.

![](./images/screenshot-01.png)

```python
try:
    # Code that might raise an exception
    x = 1 / 0
except ZeroDivisionError:
    # Code that runs if the exception occurs
    x = 0
print(x)
```

반면 위의 코드는 `x` 값을 `0`으로 설정하여 코드 실행을 계속한다.

### 여러 예외 처리
여러 개의 `except` 절을 사용하면 여러 예외를 처리할 수 있다.

```python
try:
    # Code that might raise an exception
    x = 1 / 0
except ZeroDivisionError:
    # Code that runs if ZeroDivisionError occurs
    x = 0
except TypeError:
    # Code that runs if TypeError occurs
    x = 1
```

### 중첩된 `try-except` 블럭
`try-except` 블록을 중첩하여 다양한 코드 실행 수준에서 다양한 예외를 처리할 수도 있다.

```python
import requests

def get_external_resource():
    # The URL for the API endpoint
    url = 'https://example.com'
    
    try:
        # Make the API call
        response = requests.get(url)
        
        # Check for successful status code
        if response.status_code == 200:
            return response.json()  # Assuming the API returns JSON data
        else:
            print(f"API returned an error. Status code: {response.status_code}")
            return None
    except requests.RequestException as e:
        print(f"An error occurred while making the API request: {e}")
        return None

def process_resource(external_resource):
    if external_resource is not None:
        try:
            # Replace this with actual processing code
            return external_resource / 0
        except ZeroDivisionError:
            print("ZeroDivisionError")
        except ValueError:
            print("A ValueError occurred while processing the resource.")
    else:
        print("No valid external resource to process.")
        
try:
    # Outer try block
    external_resource = get_external_resource()
    
    # Process the resource
    process_resource(external_resource)
        
except ConnectionError:
    print("A ConnectionError occurred while getting the external resource.")
except Exception as e:
    print(f"An unforeseen error occurred: {e}")
```

주어진 Python 코드에서 중첩된 `try-except` 블록은 프로그램의 여러 단계에서 예외를 처리하는 데 사용된다. 바깥 try 블록은 전체 코드를 캡슐화하여 `ConnectionError`와 기타 일반 예외를 포착한다. 이 블록 내에서 API 요청을 통해 외부 리소스를 가져오기 위해 `get_external_resource()` 함수를 호출한다. API 호출이 실패하면 오류 메시지가 출력된다. 성공하면 `process_resource()` 함수를 호출하여 가져온 리소스를 처리한다. 이 함수에는 `ZeroDivisionError`와 `ValueError`를 처리하기 위한 자체 중첩 `try-except` 블록이 있다. 이러한 특정 에러가 발생하면 해당 에러 메시지가 출력된다. 그러면 예외 발생 여부와 관계없이 프로그램 흐름이 끝으로 이동한다.

![](./images/1_ZixvwJ3180ux-pCjHBsC7A.webp)

### 내장된(Built-in) 예외 클래스
Python은 `IOError`, `ValueError`, `ZeroDivisionError`, `ImportError`, `EOFError` 등과 같은 수많은 내장된 예외를 제공한다.

- **IOError**: 이 예외는 I/O 관련 이유로 인해 I/O 작업(예: print 문, 내장된 open() 함수 또는 파일 객체의 메서드)이 실패할 때 발생한다. 예를 들어, "file not found" 또는 "disk full" 오류 등 이다.
- **ValueError**: 내장된 연산 또는 함수가 올바른 타입이지만 부적절한 값을 가진 인수를 받을 때 발생한다.
- **ZeroDivisionError**: 나눗셈 또는 모듈로 연산의 두 번째 인수가 0일 때 발생한다. 0 나눗셈은 수학적으로 정의되지 않았기 때문에 Python에서는 이 예외가 발생한다.
- **ImportError**: 모듈이 존재하지 않거나 모듈 경로가 잘못되어 Python에서 가져오려는 모듈을 찾을 수 없는 경우 Python에서 ImportError를 발생시킨다.
- **EOFError**: 내장 함수(input() 또는 raw_input()) 중 하나가 데이터를 읽지 않고 파일 종료 조건(EOF)에 도달할 때 발생한다. 이 오류는 대화형 명령 어플리케이션을 만들 때 가끔 발생한다.

### 오류 세부 정보 출력
모든 예외를 잡기 위해 일반 예외 블록을 사용하는 경우, sys 모듈의 exc_info() 함수를 사용하거나 예외 절에 변수를 사용하여 발생한 실제 예외의 세부 정보를 얻을 수 있다. 두 가지 방법을 모두 살펴보자.

- **sys.exe_info()**

```python
import sys

try:
    # some operation that raises an exception
    result = 1 / 0
except:
    exc_type, exc_obj, exc_traceback = sys.exc_info()
    print(f"An exception of type {exc_type.__name__} occurred. Details: {exc_obj}")
```

- **except절에서 변수 사용**

```python
try:
    # some operation that raises an exception
    result = 1 / 0
except Exception as e:
    print(f"An exception of type {type(e).__name__} occurred. Details: {str(e)}")
```

![](./images/1_kRUi9J_lTU3jeec3yn4EDw.webp)

FastAPI에서 자세한 사용 사례와 고급 예외 처리를 구현하는 방법에 대해 자세히 알아본다.

## FastAPI에서 커스텀 예외와 HTTP 상태 코드
FastAPI는 예외를 처리하고 적절한 HTTP 상태 코드를 반환하는 매우 간단한 방법을 제공한다. 리소스를 찾을 수 없는 경우에 대한 사용자 정의 예외가 있다고 가정하면 다음과 같다.

```python
from fastapi import HTTPException


class ResourceNotFound(HTTPException):
    def __init__(self):
        super().__init__(status_code=404, detail="Resource not found")
```

리소스를 찾을 수 없는 경우 라우트 핸들러에서 이 예외를 발생시킬 수 있다.

```python
@app.get("/items/{item_id}")
async def read_item(item_id: str):
    if item_id not in items:
        raise ResourceNotFound()
    return {"item": items[item_id]}
```

항목이 항목 dictionary에 존재하지 않으면 `ResourceNotFound` 예외가 발생하고 FastAPI는 상태 코드 404와 상세한 메시지가 포함된 본문을 포함한 응답을 반환한다.

### 코드의 여러 부분에서 예외 잡기
FastAPI에는 코드의 여러 부분에서 예외를 포착할 수 있는 미들웨어 'ExceptionMiddleware'가 있다. 이를 사용하여 컨트롤러, 모델, 서비스 등과 같은 어플리케이션의 여러 계층에서 발생하는 예외를 포착할 수 있다.

```python
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse

@app.middleware("http")
async def middleware(request: Request, call_next):
    try:
        response = await call_next(request)
        return response
    except Exception as e:
        return JSONResponse(status_code=400, content={"message": str(e)})
```

위의 코드는 요청을 실행할 미들웨어를 FastAPI에 추가하고, 요청 수명 중 어디에서든(라우트 핸들러, 종속성 등) 예외가 발생하면 예외를 포착하여 상태 코드 400과 예외 메시지가 포함된 JSON 응답을 반환한다.

### 서비스 계층에서 예외 일으키기
종종 비즈니스 로직을 서비스 계층 내부에 캡슐화하는 것이 좋은 관행이다. 사용자를 생성하는 경우를 예로 들어 보자. 사용자 이름이 고유한지 확인하고 싶을 것이다. 고유하지 않은 경우 `UsernameNotUnique` 예외를 발생시키고 싶을 것이다.

정의

```python
class UsernameNotUnique(Exception):
    """Raised when the provided username is not unique"""
    def __init__(self, username, message="Username is already in use"):
        self.username = username
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'{self.username} -> {self.message}'
```

예외 생성하기

```python
class UserService:
    @staticmethod
    async def create_user(db: Session, user: UserCreate) -> User:
        db_user = get_user_by_username(db, username=user.username)
        if db_user:
            raise UsernameNotUnique("Username already taken.")
        return create_user(db=db, user=user)
```

예외 포착

```python
@app.post("/users/")
async def create_user(user: UserCreate, db: Session = Depends(get_db)):
    try:
        return UserService.create_user(db, user)
    except UsernameNotUnique as e:
        raise HTTPException(status_code=400, detail=str(e))
```

### 커스텀 예외 가이드라인
1. **Python `Exception` 클래스로부터 상속한다**: 커스텀 예외를 만들 때, 그 예외는 항상 기본 제공 `Exception` 클래스 또는 그 하위 클래스 중 하나의 서브클래스여야 한다. 이렇게 하면 예외가 다른 예외처럼 동작하고 `try-except` 블록에서 바르게 작동할 수 있다.
2. **예외의 이름을 적절하게 지정한다**: `Exception` 클래스는 일반적으로 **"Error"**로 끝나야 하며 이름에 오류의 성격을 명확하게 나타내야 한다. 사용자 이름이 고유하지 않았을 때 오류가 발생한다는 것을 명확하게 나타내는 `UsernameNotUniqueError`를 예로 들 수 있다.
3. **유용한 오류 메시지를 제공하자**: 예외를 발생시킬 때 무엇이 잘못되었는지 설명하는 자세한 오류 메시지를 제공하는 것이 도움이 된다. 이 메시지는 예외의 `message` 속성에 저장해야 한다.
4. **`__str__` 또는 `__repr__` 메서드 재정의**: 이러한 메서드를 재정의하면 예외가 발생할 때 보다 자세한 오류 메시지를 제공하는 데 도움이 될 수 있다. `__str__` 메서드는 오류에 대한 짧고 유익한 설명을 반환해야 하며, `__repr__` 메서드는 `eval()` 함수에 전달되는 정확한 예외를 재현하는 문자열을 반환해야 한다.
1. **관련 데이터를 포함하자**: 커스텀 예외에는 예외 처리에 도움이 될 수 있는 관련 데이터를 저장할 수 있다. 예를 들어, `UsernameNotUniqueError`에 예외를 일으킨 사용자 이름을 저장할 수 있다.

```python
class UsernameNotUniqueError(Exception):
    """Raised when the provided username is not unique."""
    def __init__(self, username):
        self.username = username
        self.message = f"The username '{self.username}' is already in use."
        super().__init__(self.message)

    def __str__(self):
        return self.message

    def __repr__(self):
        return f"UsernameNotUniqueError(username={self.username!r})"
```

FastAPI의 예외 파일 구성에 대한 몇 가지 고려사항

1. **별도의 예외 파일 또는 디렉토리**: 커스텀 예외가 적은 경우 메인 어플리케이션 디렉터리에 있는 별도의 `exceptions.py` 파일에 정의할 수 있다. 커스텀 예외가 많은 경우 `exceptions` 디렉터리를 만들고 예외의 목적이나 관련 어플리케이션 부분에 따라 여러 파일로 구성하는 것이 권장된다.
2. **모델 레이어**: 앞서 언급했듯이 대부분의 커스텀 예외는 모델 계층과 관련이 있을 것이다. SQLAlchemy와 같은 ORM을 사용하는 경우, 데이터 모델과 관련된 커스텀 예외를 모델 자체와 동일한 파일에 정의하거나 모델 디렉터리 내의 별도의 `exceptions.py` 파일에 정의할 수 있다.
3. **서비스 계층**: 서비스 계층(예: "create user" 또는 "process payment"와 같은 작업을 처리하는 클래스 또는 함수)이 있는 경우 서비스 계층에서 이러한 작업과 관련된 예외를 정의할 수 있다.
- **발생된 곳과 같은 위치에서**: 또 다른 접근 방식은 커스텀 예외가 발생한 위치에 커스텀 예외를 정의하는 것이다. 예를 들어 사용자 이름 관련 연산을 처리하는 `username.py` 파일이 있는 경우 같은 파일에서 `UsernameNotUnique` 예외를 정의할 수 있다.

### 예외 처리 모범 사례

- **예외 처리의 구체성 및 세분성**<br>
예외는 항상 가능한 한 구체적으로 처리하세요. 모든 예외를 잡는 것은 일반적으로 버그를 숨기고 디버깅을 어렵게 만들 수 있으므로 좋지 않은 습관이다.

- **광범위한 예외 조항 피하기**<br>
모든 예외를 잡는 대신 코드 실행 중에 발생할 것으로 예상되는 특정 예외를 잡는 것을 목표로 한다.
- **오류 로깅과 보고**<br>
진단 목적으로 항상 오류를 로깅하자. Python에 내장된 '로깅' 모듈을 사용하여 예외가 발생할 때 기록할 수 있다.
- **우아한 성능 저하와 우아한 종료**<br>
프로그램은 항상 정상적으로 실패하여 사용자에게 무슨 일이 일어났는지 알려주고, 가능하면 오류에도 불구하고 계속 작동해야 한다. 그렇지 않은 경우 사용 중이던 모든 리소스를 정리하고 더 이상 손상을 입히지 않고 종료해야 한다.

## 고급 오류 처리 기술

- `finally` 블럭<br>
`finally` 블록은 Python의 `try/except` 구조체의 일부이다. `try` 블록에서 예외가 발생했는지 여부에 관계없이 실행되는 finally 블록이다.

```python
try:
    # attempt to open a file and write to it
    file = open('test_file.txt', 'w')
    file.write('Hello, world!')
except:
    print('An error occurred while writing to the file.')
finally:
    # this code will run whether an exception was raised or not
    file.close()
    print('The file has been closed.')
```

위의 예에서 문제가 발생할 수 있다. 이 오류는 파일을 열거나 쓰는 동안 예외가 발생하여 최종적으로 블록이 파일을 닫으려고 시도하기 전에 `file` 변수가 초기화되지 않았을 때 발생한다.

예를 들면 권한 문제, 디스크 가득 찼음, 불법 문자와 같은 파일 경로 문제, 리소스 제한, 다른 프로세스의 파일 잠금, 일반 IO 오류 등이 있다.

그렇다면 다음과 같이 처리하는 것이 더 효율적이다.

```python
file = None  # Initialize file to None so it's always defined

try:
    # Attempt to open a file and write to it
    file = open('test_file.txt', 'w')
    file.write('Hello, world!')
    print('It works!')
except:
    print('An error occurred while writing to the file.')
finally:
    # This code will run whether an exception was raised or not
    if file is not None:  # Only close if file was successfully opened
        file.close()
    print('The file has been closed.')
```

이상적으로는 위의 코드가 다음과 같이 출력된다.

```
It works!
The file has been closed.
```

- `try-except`의 `else` 절<br>
Python에서 `try/except` 블록의 `else` 절은 잘 알려지지 않았지만 자주 사용되는 기능이다. `try` 블록에서 예외가 발생하지 않는 경우에만 실행될 코드 블록을 지정할 수 있다.

```python
try:
    result = 1 / 2  # no exception raised here
except ZeroDivisionError:
    print('Divided by zero!')
else:
    print(f'The division was successful, and the result is {result}.')
```

- 제너레이터(Generators)에서 예외 처리<br>
Python의 제너레이터는 예외를 던질 수도 있다. 제너레이터 내에서 `throw()` 메서드를 사용하여 이러한 예외를 처리할 수 있다. 또한 `StopIteration` 예외를 사용하여 제너레이터를 종료할 수도 있다.

```python
def simple_generator():
    try:
        yield 'Hello, world!'
    except Exception as e:
        yield str(e)

gen = simple_generator()
print(next(gen))  # prints: Hello, world!

gen.throw(Exception('Test exception'))  # prints: Test exception
```

- 오류 처리를 위한 `contextlib` 사용<br>
안전하게 관리하고 싶은 데이터베이스 연결이 있다고 가정해 보자. 즉, 데이터베이스 작업 중에 오류가 발생하더라도 연결이 닫히지 않도록 하고 싶다는 뜻이다.
`contextlib.contextmanager`를 사용하여 데이터베이스 연결을 설정하고, 작업을 수행하기 위해 메인 코드에 제어권을 다시 넘기고, 마지막으로 연결이 닫히도록 하는 컨텍스트 관리자를 만들 수 있다. 방법은 다음과 같다.

```python
from contextlib import contextmanager

class DatabaseConnection:
    def __init__(self, name):
        self.name = name

    def close(self):
        print(f"Database {self.name} connection has been closed.")

@contextmanager
def database_connection(name):
    db = DatabaseConnection(name)  # set up the connection
    try:
        print(f"Database {db.name} connection has been established.")
        yield db  # yield control back to the main code
    finally:
        db.close()  # ensure the connection gets closed

with database_connection("test_db") as db:
    print(f"Performing operations on {db.name} database.")
```

### Tracebacks으로 디버깅
Traceback은 오류 발생 시 오류를 이해하고 진단하는 데 필수적이다. Python에서 예외가 발생하면 일반적으로 예외가 발생하게 된 일련의 함수 호출을 보여주는 Traceback이 함께 제공되므로 오류의 근본 원인을 쉽게 추적할 수 있다.

#### Traceback 이해하기
예외가 발생하면 일반적으로 Python은 예외가 발생했을 때 프로그램이 무엇을 하고 있었는지 보여주는 traceback을 stderr에 출력한다. 다음은 간단한 예이다.

```python
def function1():
    function2()

def function2():
    raise Exception('An error occurred')

function1()
```

이 스크립트를 실행하면 Python은 다음과 같은 trackback을 출력한다.

```
Traceback (most recent call last):
  File "script.py", line 7, in <module>
    function1()
  File "script.py", line 2, in function1
    function2()
  File "script.py", line 5, in function2
    raise Exception('An error occurred')
Exception: An error occurred
```

traceback을 보면 메인 모듈(`<module>`)에서 호출된 `function1`에서 호출된 `function2`에서 오류가 발생했음을 알 수 있다.

그러면 `function2`에서 발생한 예외를 처리할 수 있다.

```python
def function1():
    try:
        function2()
    except Exception as e:
        print(e)

def function2():
    raise Exception('An error occurred')

function1()
```

#### 프로그래밍 방식으로 Tracebaks과 상호 작용
Python에 내장된 `traceback` 모듈을 사용하면 프로그래밍 방식으로 traceback 작업을 할 수 있다. 예를 들어 traceback을 캡처한 다음 나중에 직접 인쇄할 수 있다.

```python
import traceback

try:
    function1()
except Exception:
    tb = traceback.format_exc()

print("Here's the traceback:")
print(tb)
```

이 예에서 `traceback.format_exc()`는 현재 예외와 traceback에 대한 문자열을 반환하며, 이를 출력하거나 원한다면 로깅도 할 수 있다.

#### FastAPI에서 tracebacks
FastAPI에서 오류가 처리되지 않으면 오류 세부 정보가 포함된 JSON 응답을 반환한다. FastAPI 어플리케이션을 만들 때 `debug=True`로 설정하면 이러한 오류 세부 정보가 traceback을 포함한다.

```python
from fastapi import FastAPI

app = FastAPI(debug=True)

@app.get("/")
def root():
    function1()
```

이 엔드포인트를 방문했을 때 오류가 발생하면 JSON 응답에는 traceback이 있는 "traceback" 필드를 포함한다. traceback에는 민감한 정보가 포함될 수 있으므로 이 필드는 개발에만 사용해야 한다는 점을 유의하자.

### 다른 디버깅 기술
디버깅은 Python 코드를 작성하고 유지 관리하는 데 있어 필수적인 측면이다. 다행히도 Python은 프로그래머가 버그를 식별하고 수정하는 데 도움이 되는 몇 가지 도구와 기법을 제공한다. 그 중 몇 가지를 살펴보겠다.

#### pdb - Python 디버거
Python Debugger(pdb)는 Python에 포함된 강력한 대화형 디버거이다. pdb를 사용하여 코드를 단계별로 살펴보고, 데이터를 검사하고, 프로그램 실행을 일시 중지하면서 실행 환경을 사용할 수 있다. 다음은 기본적인 예이다.

```python
import pdb

def calculate_sum(a, b):
    pdb.set_trace()  # This will pause the program's execution
    return a + b

calculate_sum(1, '2')  # This will cause a TypeError
```

이 스크립트를 실행하면 `pdb.set_trace()`에서 실행이 일시 중지되며, 변수를 검사하거나 다음 줄로 넘어갈 수 있다.


> **Note**:
> `pdb.set_trace()`는 Python 3.7부터 더 이상 사용되지 않는다는 것을 알고 있어야 한다. Python 3.7 이상을 사용하는 경우 대신 `breakpoint()`를 사용해야 한다. 코드에서 `pdb.set_trace()`를 `breakpoint()`로 바꾸기만 하면 된다.

```python
def calculate_sum(a, b):
    breakpoint()  # This will pause the program's execution
    return a + b

calculate_sum(1, '2')  # This will cause a TypeError
```

#### IDE와 Editor에서 디버깅
대부분의 최신 IDE(Integrated Development Environment)와 코드 편집기는 강력한 디버깅 기능을 제공한다. 예를 들어 PyCharm에는 사용자 친화적인 인터페이스에서 코드를 단계별로 살펴보고, breakpoint을 설정하고, 변수를 검사하고, 표현식을 평가하는 등의 작업을 수행할 수 있는 모든 기능을 갖춘 디버거가 있다. 또한 VS Code는 Python extension 기능을 통해 뛰어난 디버깅 기능을 제공한다.

#### 디버깅을 위한 로깅
로깅은 실행 중 코드에서 무슨 일이 일어나는지 파악하는 강력한 도구이다. Python에 내장된 'logginh' 모듈을 사용하면 다양한 심각도 수준(DEBUG, INFO, WARNING, ERROR, CRITICAL)으로 메시지를 기록할 수 있다. 로그 메시지는 변수 상태나 함수 호출 순서와 같은 프로그램 실행에 대한 유용한 정보를 포함할 수 있다. 다음은 예이다.

```python
import logging

logging.basicConfig(level=logging.DEBUG)  # Set the log level to DEBUG

def calculate_sum(a, b):
    logging.debug(f'calculate_sum({a}, {b})')
    return a + b

calculate_sum(1, 2)
```

이 스크립트를 실행하면 콘솔에 "DEBUG:root:calculate_sum(1, 2)" 메시지를 출력한다. 이 메시지는 프로그램의 흐름과 변수 값을 이해하는 데 도움이 될 수 있다.

### 프로덕션 코드에서 예외 처리
- **웹 어플리케이션의 오류 처리**<br>
웹 어플리케이션, 특히 FastAPI는 서버 충돌을 방지하고 사용자 경험을 향상시키기 위해 예외를 처리해야 한다. FastAPI는 `HTTPException`을 사용하여 예외를 처리할 수 있다.
- **데이터 파이프라인의 오류 처리**<br>
데이터 파이프라인은 데이터 손상이나 손실을 방지하기 위해 예외를 처리해야 합니다.
- **비동기 코드의 오류 처리(asyncio)**<br>
비동기 코드의 예외 처리는 일반 Python 코드와 비슷하지만, 비동기적 특성으로 인해 몇 가지 특별한 고려 사항이 있다.

### 테스팅과 오류 처리
- **예외 시나리오를 위한 단위 테스트**<br>
다양한 예외 시나리오를 다루기 위해 단위 테스트를 작성해야 한다. Python의 `unittest` 모듈은 특정 예외가 발생하는지 테스트하는 `assertRaises` 메서드를 제공한다.
- **테스트에서 예외 모킹하기**<br>
테스트에서 예외 시나리오를 시뮬레이션하려면 모킹을 사용할 수 있다.

### 예외 메시지의 모범사례
- **명확하고 유익한 오류 메시지**<br>
오류 메시지는 명확하고 유익한 정보를 제공하여 사용자나 개발자가 무엇이 잘못되었는지, 어떻게 해결할 수 있는지 이해할 수 있도록 안내해야 한다.
- **국제화와 오류 메시지**
해외 사용자를 위한 소프트웨어를 개발하는 경우 오류 메시지 번역을 고려하자.
- **오류 메시지의 일관성**
어플리케이션 전체에서 오류 메시지의 일관성을 유지하는 것은 디버깅을 더 쉽게 만드는 좋은 습관이다.

### 오류 처리에서 성능 고려사항
- **예외 처리가 성능에 미치는 영향**<br>
예외 처리는 특히 예외가 자주 발생하는 경우 Python 프로그램의 성능에 영향을 미칠 수 있다.
- **예외 처리와 조건부 처리 비교**<br>
오류 처리를 위해 예외를 사용할지, 조건부 검사를 사용할지는 오류의 성격과 프로그램의 아키텍처에 따라 중요한 결정이 될 수 있다.

### 결론
지금까지 Python에서 오류 처리의 중요성, 오류 타입, 예외 처리 메커니즘, 모범 사례, 고급 기법, 방어 프로그래밍 및 테스트에 대해 논의했다.

Python 프로젝트에서 강력한 오류 처리 기술을 이해하고 구현하면 코드 품질, 사용자 경험 및 유지 관리 가능성을 개선할 수 있다.

예외 처리는 프로그래밍의 다른 측면과 마찬가지로 지속적인 학습과 개선이 필요한 영역이다. 배우고 적용해야 할 새로운 기술, 도구, 모범 사례는 항상 존재한다.

이 글이 Python에서 예외 처리를 마스터하는 데 도움이 되기를 바라며, 특히 FastAPI를 사용하는 전문가에게 도움이 되기를 바fks다. 즐거운 코딩 되세요!


<a name="footnote_1">1</a>: 이 페이지는 [Exception & Error Handling in Python for Professionals: A Comprehensive Guide with FastAPI](https://blog.gopenai.com/exception-error-handling-in-python-for-professionals-a-comprehensive-guide-with-fastapi-3ef57de8e796)을 편역한 것임.