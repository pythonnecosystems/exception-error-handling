# Exception & Error Handling

Exception & Error Handling라는 주제는 나를 항상 괴롭현던 것 중 하나이다. 오랜만에 관심있는 주제를 만나 정리를 시작한다. 그 첫번째는 [전문가를 위한 Python 예외와 오류 처리: FastAPI에서의 가이드](./comprehensive-guide-with-fastapi.md)로 [Exception & Error Handling in Python for Professionals: A Comprehensive Guide with FastAPI](https://blog.gopenai.com/exception-error-handling-in-python-for-professionals-a-comprehensive-guide-with-fastapi-3ef57de8e796)를 편역한 것이다.
